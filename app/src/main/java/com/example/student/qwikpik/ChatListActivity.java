package com.example.student.qwikpik;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ChatListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_chat);

        ChatListFragment chatList = new ChatListFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, chatList).commit();
    }
}

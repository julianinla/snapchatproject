package com.example.student.qwikpik;

/**
 * Created by Student on 5/24/2016.
 */
public class Constants {

    public static final String ACTION_ADD_FRIEND = "com.example.student.qwikpik.ADD_FRIEND";
    public static final String ACTION_SEND_FRIEND_REQUEST = "com.example.sam.demo.SEND_FRIEND_REQUEST";

    public static final String BROADCAST_ADD_FRIEND_SUCCESS = "com.example.sam.demo.ADD_FRIEND_SUCCESS";
    public static final String BROADCAST_ADD_FRIEND_FAILURE = "com.example.sam.demo.ADD_FRIEND_FAILURE";

    public static final String BROADCAST_FRIEND_REQUEST_SUCCESS = "com.example.sam.demo.FRIEND_REQUEST_SUCCESS";
    public static final String BROADCAST_FRIEND_REQUEST_FAILURE = "com.example.sam.demo.FRIEND_REQUEST_FAILURE";

}

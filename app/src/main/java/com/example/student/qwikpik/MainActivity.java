package com.example.student.qwikpik;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity {

    public static final String APP_ID ="524A0B5F-F5D1-2EE2-FF23-6F3858880300";
    public static final String SECRET_KEY = "B42E677A-CB1B-078D-FF34-73297B8F0700";
    public static final String VERSION = "v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        if(Backendless.UserService.loggedInUser().equals("")){
            RegisterFragment regPage = new RegisterFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, regPage).commit();
        }
        /* else if(loggedinuser && no friends) {
            FriendsListFragment friendsList = new FriendsListFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.registerContainer, friendsList).commit();
        }
         */
        else {
            ChatListFragment chatView = new ChatListFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, chatView).commit();
        }
    }
}

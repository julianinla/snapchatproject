package com.example.student.qwikpik;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ViewRequestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_request);

        ViewRequestFragment viewRequest = new ViewRequestFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, viewRequest).commit();
    }
}

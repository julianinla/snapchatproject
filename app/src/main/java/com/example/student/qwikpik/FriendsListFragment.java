package com.example.student.qwikpik;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FriendsListFragment extends Fragment {

    private ArrayList<String> friends;
    private ArrayAdapter<String> friendsListAdapter;

    public FriendsListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_friend_list, container, false);

        ListView listFriend = (ListView) view.findViewById(R.id.listFriend);

        friends = new ArrayList<String>();
        friendsListAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, friends);

        listFriend.setAdapter(friendsListAdapter);

        String currentUser = Backendless.UserService.loggedInUser();
        Backendless.Persistence.of(BackendlessUser.class).findById(currentUser, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser user) {
                Object[] friendObjects = (Object[]) user.getProperty("friends");
                if (friendObjects.length > 0) {
                    BackendlessUser[] friendArray = (BackendlessUser[]) friendObjects;
                    for (BackendlessUser friend : friendArray) {
                        String name = friend.getProperty("name").toString();
                        friends.add(name);
                        friendsListAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });

        Button addFriendButton = (Button) view.findViewById(R.id.addFriend);
        addFriendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setMessage("Add a Friend.");

                final EditText inputField = new EditText(getActivity());
                alertDialog.setView(inputField);

                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do nothing
                    }
                });

                alertDialog.setPositiveButton("Add Friend", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendFriendRequest(inputField.getText().toString());
                        //Toast.makeText(getActivity(), "it works", Toast.LENGTH_SHORT).show();
                    }
                });

                alertDialog.create();
                alertDialog.show();
            }
        });

        Button requestLink = (Button) view.findViewById(R.id.viewRequests);
        requestLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ViewRequestActivity.class);
                startActivity(intent);
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    private void sendFriendRequest(final String friendName) {
        String currentUserID = Backendless.UserService.loggedInUser();
        Backendless.Persistence.of(BackendlessUser.class).findById(currentUserID, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser currUser) {
                Intent intent = new Intent(getActivity(), FriendService.class);
                intent.setAction(Constants.ACTION_SEND_FRIEND_REQUEST);
                intent.putExtra("fromUser", currUser.getProperty("name").toString());
                intent.putExtra("toUser", friendName);

                getActivity().startService(intent);
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });
    }

}

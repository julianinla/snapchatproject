package com.example.student.qwikpik;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ListFriendActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_friend);

        FriendsListFragment listFriends = new FriendsListFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, listFriends).commit();
    }
}
